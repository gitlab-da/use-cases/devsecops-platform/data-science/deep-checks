import os
import streamlit as st
from pathlib import Path
import streamlit.components.v1 as components

add_selectbox = st.sidebar.selectbox(
    "Model performance checks",
    ("Pass", "Didn't pass", "Need more attention")
)

# Using "with" notation
with st.sidebar:
    add_radio = st.radio(
        "Choose a test suite",
        ("Full suite", "Partial testing")
    )

script_dir = os.path.dirname(os.path.abspath(__file__))
report_path = os.path.join(script_dir, '..', 'src', 'my_results.html')


with open(report_path, 'r') as html_file:
    html_code = html_file.read()

tab_1, tab_2 = st.tabs(["📊 Analytics Reports ", " 📝 Reports "])

with tab_1:
    components.html(html_code, height=1000, width=1000)

with tab_2:
    st.write("reports")
